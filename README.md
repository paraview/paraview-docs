Introduction
============

This project contains the source files used to generate ParaView Documentation hosted at [docs.paraview.org][ParaViewDocs]. This documentation includes the ParaView User's Guide, ParaView Reference Manual, and ParaView Tutorials.

Documentation is written in the [reStructuredText][] format. Images are stored in PNG or JPEG format in the `doc/source/images` directory.

Software Requirements
=============

Building the documentation is currently supported on Linux and macOS platforms. Certain software is required to be installed to generate the documentation, including:

* python
* pip
* Python packages listed in the `requirements.txt` file in the `doc` directory

To install the required Python packages with `pip`, use

```
pip install -r requirements.txt
```

Building
========

To build the documentation, first clone the git repository with

```
git clone git@gitlab.kitware.com:paraview/paraview-docs.git
```

Descend into the `paraview-docs/docs` directory.

```
cd paraview-docs/docs
```

Now run `make` and provide a build target. To list available targets, simply run `make`. For generating HTML help, run

```
make html
```

Build products will be generated under the `build/html` directory. Generated documentation can be viewed by opening the `index.html` file in that directory.

License
=======

ParaView Documentation is distributed under the Creative Commons Attribution-NoDerivatives 4.0 International Public License. See [License.txt][] for details.

[ParaViewDocs]: https://docs.paraview.org/

[reStructuredText]: https://docutils.sourceforge.io/rst.html

[License.txt]: License.txt

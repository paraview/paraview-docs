Debugging Tips
##############

Printing Logs
=============

ParaView Catalyst uses the standard logging system of ParaView to log its internal execution.
To increase the verbosity of those specific logs, you can set the ``PARAVIEW_LOG_CATALYST_VERBOSITY``
environmental variable to `INFO`.

If you have issues prior to the actual ParaView Catalyst execution, you can set ``CATALYST_DEBUG`` to
any non empty value. This will enable upstream logs about the Catalyst implementation loading procedure.
You will see which implementation is actually loaded.

Debugging Runtime
=================

Dealing with issues during the in situ analysis can be painful as you need to run the whole simulation code
at each new try.
Fortunately, two useful tools exist to help debugging your runtime.

First is the `Catalyst Replay`_. With that, you need to run your simulation only once:
it will write reusable data on your disk. Then, you only need to call the ``catalyst_replay``
executable to simulate a new run. Some environment variables may be useful here:

* ``CATALYST_IMPLEMENTATION_PATHS`` and ``CATALYST_IMPLEMENTATION_NAME`` to point to the desired Catalyst implementation.
* ``CATALYST_IMPLEMENTATION_PREFER_ENV`` to give priority to environment variable when looking for Catalyst implementation.
* ``PYTHONPATH`` to override the python script directory, as Catalyst Replay will store an absolute path.

You can also emulate a simulation from a time series of VTK files, using the `Catalyst Player`_.
This is useful to prototype an analysis script from a representative dataset.

.. _Catalyst Replay: https://catalyst-in-situ.readthedocs.io/en/latest/catalyst_replay.html
.. _Catalyst Player: https://gitlab.kitware.com/paraview/catalyst-player

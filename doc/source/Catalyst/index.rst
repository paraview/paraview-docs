.. _chapter:Catalyst:

Catalyst
========

Catalyst is an **in situ framework** created as part of ParaView a few
years ago.

Also known as coprocessing or concurrent analysis, the in situ workflow allows
you to perform **analysis and visualization** at the time of the simulation.

As only the output of the analysis is written on the disk, the **I/O** operations
are **considerably reduced** compared to:

- writing full simulation output
- reading the full-size data in an analysis tool
- writing the analysis output.

ParaView Catalyst provides a small, easy-to-use, API that any simulation
developed in C++, C, Fortran or Python can use to do in situ analysis without developing
its own custom data analysis code.

Simply give it a ParaView Python pipeline and your input data!

.. toctree::
   :maxdepth: 2

   introduction
   getting_started
   background
   blueprints
   fides
   examples
   debugging

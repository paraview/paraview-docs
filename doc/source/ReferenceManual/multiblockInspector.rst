.. include:: ../macros.hrst
.. include:: ../abbreviations.hrst

.. _chapter:MultiblockInspector:

Multiblock Inspector
####################

Composite datasets (:numref:`chapter:UnderstandingData`) such as partitioned
dataset collections, multiblock datasets and AMR are often encountered when
visualizing results from several scientific simulation codes e.g. OpenFOAM,
Exodus, etc. Readers for several of these simulation file formats support
selecting which blocks to read. Additionally, you may want to control display
properties such as visibility, color, color array, use separate color map,
interpolate scalars before mapping, opacity, and map scalars for individual
blocks or sub-trees. You can use the ``Multiblock Inspector``  panel for this.

:numref:`fig:MultiblockInspector` shows the  ``Multiblock Inspector``   showing the
hierarchy from an Exodus dataset. The panel tracks the active source and reflects the
structure for the data produced by the active source. The display properties reflect
their state in the active view.


.. figure:: ../images/MultiblockInspector.png
    :name: fig:MultiblockInspector
    :width: 80%
    :align: center

    The  ``Multiblock Inspector``  

To show or hide a block you have to click on the checkbox next to the
block name. Toggling the visibility of a non-leaf block, affects the
visibility of the entire sub-tree.

The first column reflects the state of all properties used for the block.
and the second column is a reset button that allows you to reset all
properties for the block to the default values. Block properties can
be specified in multiple ways. First, you can choose to not use any block
specific overrides and simply let the default display properties (
:numref:`sec:DisplayingData:DisplayProperties`) affect the
rendering. This is the default behavior. When that is the case for any block,
it is indicated in the state column with |pqStateDisabled|, if no block is selected,
and with |pqStateRepresentationInherited| if a block is selected. Second, you can
explicitly override the display of any block or a subtree. To do this, simply select
a block or blocks in the multiblock inspector, and change any display property as you
would in the ``Properties`` panel. Upon changing any property, the state column will
reflect the change. If the property is set for the block, the state column will show
|pqStateSet|. When an explicit property is set on a non-leaf node, all its children
(and their children) inherit that property unless explicitly overridden. For such nodes
that have a property inherited from their parent, we use the icon |pqStateBlockInherited|.
Since a block has many properties, or many blocks might have been selected, we also
show a few more icons to capture all possible states. |pqStateMixedInherited| is
shown when at least one property is inherited from block(s) and the representation.
|pqStateSetAndRepresentationInherited| is shown when at least one property is explicitly set and
inherited from the representation. |pqStateSetAndBlockInherited| is shown when at least one property is
explicitly set and inherited from the block(s). |pqStateSetAndMixedInherited| is shown
when at least one property is explicitly set and inherited from block(s) and the representation.

Each property or widget that captures a collection of properties also has a state icon
and a reset button specific to that property or collection of properties.

.. admonition:: **Did you know?**
   :class: tip

   In most cases with multiblock datasets, ParaView uses the  ``vtkBlockColors``  
   array for coloring. This is an array filled with discrete values so that each
   block can be colored using a different color. That makes it easier to visually
   see each of the blocks in the view.

Besides using the  ``Multiblock Inspector``   to set color and opacity overrides
for blocks, you can also directly change these parameters from the   ``Render View``
itself. Simply right-click in the render view on the block of interest and
you'll get a context menu,  :numref:`fig:RenderViewContextMenu`, that allows
changing some block properties and more.

.. figure:: ../images/RenderViewContextMenu.png
    :name: fig:RenderViewContextMenu
    :width: 80%
    :align: center

    Context menu in  ``Render View``   can be used to change block display
    properties.


Another useful feature with the  ``Multiblock Inspector``   is selection. If you
simply click on any row in the inspector, you will select the block (or
subtree) and the active view will highlight the selected block(s). Conversely,
if you make a block-based selection in the active ``Render View``
using |pqSelectBlock| or |pqFrustumSelectionBlock|, you will see the
corresponding blocks highlighted in the  ``Multiblock Inspector``  panel.


.. |pqSelectBlock| image:: ../images/icons/pqSelectBlock.svg
   :width: 20px

.. |pqFrustumSelectionBlock| image:: ../images/icons/pqFrustumSelectionBlock.svg
   :width: 20px

.. |pqStateDisabled| image:: ../images/icons/pqStateDisabled.svg
   :width: 20px

.. |pqStateRepresentationInherited| image:: ../images/icons/pqStateRepresentationInherited.svg
   :width: 20px

.. |pqStateBlockInherited| image:: ../images/icons/pqStateBlockInherited.svg
   :width: 20px

.. |pqStateMixedInherited| image:: ../images/icons/pqStateMixedInherited.svg
   :width: 20px

.. |pqStateSet| image:: ../images/icons/pqStateSet.svg
   :width: 20px

.. |pqStateSetAndRepresentationInherited| image:: ../images/icons/pqStateSetAndRepresentationInherited.svg
   :width: 20px

.. |pqStateSetAndBlockInherited| image:: ../images/icons/pqStateSetAndBlockInherited.svg
   :width: 20px

.. |pqStateSetAndMixedInherited| image:: ../images/icons/pqStateSetAndMixedInherited.svg
   :width: 20px

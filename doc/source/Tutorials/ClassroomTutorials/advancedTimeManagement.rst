.. include:: ../../macros.hrst
.. include:: ../../abbreviations.hrst

.. _chapter:AdvancedTimeManagement:

Advanced: Time Management
#########################

Introduction
============

This tutorial deals with advanced topics in time management.

The Time Manager View
=====================

-  Open can.ex2.
-  **Apply**..
-  Drag the can around with the left mouse button.
-  Move forward to timestep number 5.
-  Set **Coloring** to **DISPL**.

   -  Note: If you changed the order of the steps above, you may need to click **Rescale to Data Range**.
-  **View → Time Manager**.
-  Grab the slider bar on the can.ex2 row and move it back and forth.

.. figure:: ../../images/Advanced_time_manager_1.png
   :width: 1000px


Temporal Interpolator
=====================

-  Let's change the mode to Sequence, and the Nunber of Frames to 200.
-  **Filters → Temporal → Temporal Interpolator**.
-  **Apply**..
-  In the Time Manager View window, unselect the checkbox in front of the TemporalInterpolator.  This turns off reading time from this filter.
-  Change the number of frames to 200
-  **Play**. Notice that the can motion is now smooth. |paraview| is
   interpolating between frames, and making 200 time steps.

   -  Note that this only works with data that has a constant mesh through
      the whole time sequence. AMR (Adaptive Mesh Refinement) data does
      not work with the Temporal Interpolator.

.. figure:: ../../images/Advanced_time_manager_2.png
   :width: 1000px

-  Delete the **Temporal Interpolator**

Move the camera
===============

There are three ways to move the camera. they are:

-  Follow Data
-  Interpolate Camera Locations
-  Follow Path

Camera Follow Data
------------------

We are going to make the camera follow the data.

-  **Edit → Reset Session**.
-  Open can.ex2.
-  **Apply**.
-  **+Y**.
-  **View → Time Manager**.
-  Add a camera. Next to Animations, Change **can.ex2** to **Camera**.  Change ""Follow Path** to **Follow Data**.
-  Click the blue **+**.
-  **Play**.

.. admonition:: **Did you know?**
   :class: tip

   The follow data option will follow the data from whatever filter is highlighted.
   This means that you can choose one cell, run the **Extract Selection** filter,
   and follow this cell. You do not need to keep visibility on for this cell.

Interpolate Camera Location
---------------------------

We are going to move the camera along a straight line. We want to move
the camera to follow the can.

-  **Edit → Reset Session**.
-  Open can.ex2.
-  **Apply**.
-  **+Y**.
-  **View → Time Manager**.
-  Add a camera. Next to Animations, Change **can.ex2** to **Camera**.  Change ""Follow Path** to **Interpolate Camera**.
-  Click the blue **+**.
-  Double left click on the white section of the camera row that just appeared.
-  An **Animation Keyframes** dialog will open.
-  Left click the top **Position**.
-  Use Current.
-  **OK**
-  **Play** to the last timestep.
-  Move the can dataset back into the center of the screen. **Tip** - Don't hit **Reset**.
-  Left click the bottom **Position**.
-  Use Current.
-  **OK**.
-  **OK**.
-  **Play**.

You can also create an intermediate destination for the can by going to
the Animation Keyframes dialog, and selecting New. Then, follow the
directions above. Experiment by adding more keyframes and different
camera angles.

A way to create tracks in 3d space for use with Interpolate Camera
Location is to create the points external to Interpolate Camera Location,
and then copy them into place.  One way to create these external points is
to use a Sources/ Plane.  Orient the plane in the 2d plane you want the
camera to track.  Now, create a Spline source.  Create additional control
points, and selecting each control point, use the **p** key to place them
on the plane.  Copy these points into the Interpolate Camera Location
controls.  Note that copying currently has to be done by hand.


Follow Path
-----------

We are going to move the camera along a spline. Advanced operation.

-  **Edit → Reset Session**.
-  Open can.ex2.
-  **Apply**.
-  **+Y**.
-  **View → Time Manager**.
-  Add a camera. Next to Animations, Change **can.ex2** to **Camera**.  Change ""Follow Path** to **Follow Path**.
-  Click the blue **+**.
-  Left click on the white section of the camera row that just appeared.
-  An **Animation Keyframes** dialog will open.
-  Left click on **Path**.
-  Click on **Camera Position**.
-  Now, in the 3d view, zoom out.
-  Rotate the can. You will see a yellow circle with white spheres.
-  Drag the spheres around.
-  **OK**.
-  **OK**.
-  **Play**.

One way to create tracks in 3d space  for **Follow Path** is to use
the same procedure listed above for **Interpolate Camera Location**.

.. figure:: ../../images/Advanced_time_manager_3.png
   :width: 1000px

Animating a Filter
==================

In the previous example we showed how to manipulate the camera using the
Time Manager View tools. In this example we will show how to animate a
filter. Our goal is to move a slice through our dataset over time.

-  Lets start from scratch. One way is to go **Edit → Reset Session**.
-  **Ok**.
-  If you closed it, bring up the **Time Manager View**.
-  Open disk_out_ref.ex2.
-  **Apply**.
-  Select **Slice** filter.
-  **Apply**.
-  Turn off the **Show Plane**.
-  Rotate the slice slightly so we can see it.
-  In the **Time Manager View**, unselect disk_out_ref.exo, and change the number of
   frames to 400.
-  We want to create a **Slice** track.
-  Slice Offset Values.
-  Click the blue **+**.
-  Double click on the track. This will bring up a dialog, and will set
   the start and end.
-  Change the starting value to **-8** and the ending value to **8**.
-  **Play**.

.. figure:: ../../images/Advanced_time_manager_4.png
   :width: 1000px

Data Animation
==============

In this example we will show how to animate your dataset. Our goal is
to show one dataset, then fade into another dataset. This can be handy
when one physics simulation runs for an early time period, and another
physics simulation runs for the later time period.

-  Lets start from scratch. One way is to go **Edit → Reset Session**.
-  **OK**.
-  If you closed it, bring up the **Time Manager View**.
-  Open can.ex2.
-  **Apply**.
-  Open can.ex2 again.
-  **Apply**.
-  Select the upper can.ex2.
-  Set **Coloring** to **DISPL**.
-  Go the last time step.
-  Rescale to Data Range.
-  Go to first time step.
-  Select the lower can.ex2.
-  Change the representation to **Wireframe**.

We now want to fade from the first version of the can to the second
version of the can. This is done as follows:

-  On the **Time Manager View**, on the **Animations** line, on the upper can.ex2 pulldown menu, select the upper
   can.ex2.
-  Right of there, use the pulldown menu to select Opacity.
-  Click the blue **+**.
-  Do the same for the lower can.ex2.
-  Click on the upper can.ex2 white horizontal bar.
-  Double click on the upper value, change it to 1.
-  Double click the lower value, change it to 0.
-  **OK**.
-  **Play**.

You can substitute Visibility for Opacity when you add tracks to the
Animation View. Then, on one dataset, you can run visibility of 1 for
half of your time, and run visibility of 1 for the other dataset for the
second half of your time. Thus, you will show the first simulation for
the first half of your animation, and the second simulation for the
second half.

Warp Vector Filter
==================

-  If your dataset has displacement data, but it is not using a variable
   name that ParaView recognizes, you can still animate your data.
   Choose the **Filters → Alphabetical → Warp Vector filter**.

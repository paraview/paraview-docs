.. include:: ../macros.hrst

ParaView Tutorials
==================

**Tutorials** are split in **Self-directed Tutorials** and **Classroom Tutorials**:

   **Self-directed Tutorial**'s :numref:`chapter:SelfDirectedTutorialIntroduction` to
   :numref:`chapter:FurtherReading` provide an introduction to the |ParaView| software and its history,
   and exercises on how to use |ParaView| that cover basic usage, batch Python scripting and visualizing large models.

   **Classroom Tutorials**'s :numref:`chapter:BeginningParaView` to :numref:`chapter:TargetedTrame` provide
   beginning, advanced, Python and batch, and targeted tutorial lessons on how to use |ParaView| that are presented
   as a 3-hour class internally within Sandia National Laboratories.

.. toctree::
   :maxdepth: 2
   :caption: Self-directed Tutorial

   SelfDirectedTutorial/index.rst

.. toctree::
   :maxdepth: 2
   :caption: Classroom Tutorials

   ClassroomTutorials/index.rst
